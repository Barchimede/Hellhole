using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hellhole.XUnitTest
{
    public class BandsApiControllerTests
    {
        [Fact]
        public async Task TestGetBands()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            BandsApiController tested = new BandsApiController(dbContextMock.Object);

            // Act
            IEnumerable<Band> actionResult = tested.GetBands();

            // Assert
            Assert.Null(actionResult);
        }

        [Fact]
        public async Task TestGetBand()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            BandsApiController tested = new BandsApiController(dbContextMock.Object);

            // Act
            Band test = new Band();
            IActionResult actionResult = await tested.GetBand(0);

            // Assert
            Assert.Null(actionResult);
        }

    }
}
