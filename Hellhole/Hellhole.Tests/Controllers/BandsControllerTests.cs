using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;

namespace Hellhole.XUnitTest
{
    public class BandsControllerTests
    {
        [Fact]
        public async void TestIndex()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            BandsController tested = new BandsController(dbContextMock.Object, environmentMock.Object);

            // Act
            IActionResult result = await tested.Index();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestDetails()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            BandsController tested = new BandsController(dbContextMock.Object, environmentMock.Object);

            // Act
            IActionResult result = await tested.Details(0);
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public void TestCreate()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            BandsController tested = new BandsController(dbContextMock.Object, environmentMock.Object);

            // Act
            IActionResult result = tested.Create();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestCreateWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            BandsController tested = new BandsController(dbContextMock.Object, environmentMock.Object);

            // Act
            Band toAdd = new Band();
            IActionResult result = await tested.Create(toAdd, null);

            ViewResult viewResult = Assert.IsType<ViewResult>(result);
            Band model = Assert.IsType<Band>(viewResult.ViewData.Model);

            // Assert
            Assert.Equal(toAdd.GetHashCode().ToString(), model.GetHashCode().ToString());
        }

        [Fact]
        public async void TestEdit()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            BandsController tested = new BandsController(dbContextMock.Object, environmentMock.Object);

            // Act
            Band toAdd = new Band();
            toAdd.Name = "AAA";
            IActionResult result = await tested.Create(toAdd, null);
            result = await tested.Edit(0);

            ViewResult viewResult = Assert.IsType<ViewResult>(result);
            Band model = Assert.IsType<Band>(viewResult.ViewData.Model);

            // Assert
            Assert.Equal("AAA", model.Name);
        }

        [Fact]
        public async void TestEditWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            BandsController tested = new BandsController(dbContextMock.Object, environmentMock.Object);

            // Act
            Band toAdd = new Band();
            toAdd.Name = "AAA";
            IActionResult result = await tested.Create(toAdd, null);
            toAdd.Name = "BBB";
            result = await tested.Edit(0, toAdd);

            ViewResult viewResult = Assert.IsType<ViewResult>(result);
            Band model = Assert.IsType<Band>(viewResult.ViewData.Model);

            // Assert
            Assert.Equal("BBB", model.Name);
        }

    }
}
