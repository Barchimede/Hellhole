using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using System.Collections.Generic;
using Hellhole.WebUI.Models;

namespace Hellhole.XUnitTest
{
    public class EventsApiControllerTests
    {
        [Fact]
        public async Task TestGetEvents()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            EventsApiController tested = new EventsApiController(dbContextMock.Object);

            // Act
            IEnumerable<Event> actionResult = tested.GetEvents();

            // Assert
            Assert.Null(actionResult);
        }

        [Fact]
        public async Task TestGetEvent()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            EventsApiController tested = new EventsApiController(dbContextMock.Object);

            // Act
            IActionResult actionResult = await tested.GetEvent(0);

            // Assert
            Assert.Null(actionResult);
        }

        [Fact]
        public async Task TestGetNextEventListAscending()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            EventsApiController tested = new EventsApiController(dbContextMock.Object);

            // Act
            EventsList actionResult = tested.GetNextEventListAscending();

            // Assert
            Assert.Null(actionResult);
        }

        [Fact]
        public async Task TestGetPreviousEventListDescending()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            EventsApiController tested = new EventsApiController(dbContextMock.Object);

            // Act
            EventsList actionResult = tested.GetPreviousEventListDescending();

            // Assert
            Assert.Null(actionResult);
        }

    }
}
