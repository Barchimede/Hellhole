using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;

namespace Hellhole.XUnitTest
{
    public class EventsControllerTests
    {
        [Fact]
        public async void TestIndex()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            EventsController tested = new EventsController(dbContextMock.Object, environmentMock.Object);

            // Act
            IActionResult result = await tested.Index();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestDetails()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            EventsController tested = new EventsController(dbContextMock.Object, environmentMock.Object);

            // Act
            IActionResult result = await tested.Details(0);
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public void TestCreate()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            EventsController tested = new EventsController(dbContextMock.Object, environmentMock.Object);

            // Act
            IActionResult result = tested.Create();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestCreateWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            EventsController tested = new EventsController(dbContextMock.Object, environmentMock.Object);

            // Act
            Event toAdd = new Event();
            IActionResult result = await tested.Create(toAdd, null);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async void TestEdit()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            EventsController tested = new EventsController(dbContextMock.Object, environmentMock.Object);

            // Act
            Event toAdd = new Event();
            IActionResult result = await tested.Create(toAdd, null);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async void TestEditWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            EventsController tested = new EventsController(dbContextMock.Object, environmentMock.Object);

            // Act
            Event toAdd = new Event();
            IActionResult result = await tested.Create(toAdd, null);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

    }
}
