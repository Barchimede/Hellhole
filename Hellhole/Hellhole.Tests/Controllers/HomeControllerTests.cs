using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;

namespace Hellhole.XUnitTest
{
    public class HomeControllerTests
    {
        [Fact]
        public void TestIndex()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            HomeController tested = new HomeController();

            // Act
            IActionResult result = tested.Index();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void TestAbout()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            HomeController tested = new HomeController();

            // Act
            IActionResult result = tested.About();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void TestContact()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            HomeController tested = new HomeController();

            // Act
            IActionResult result = tested.Contact();

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void TestError()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            HomeController tested = new HomeController();

            // Act
            IActionResult result = tested.Error();

            // Assert
            Assert.Null(result);
        }

    }
}
