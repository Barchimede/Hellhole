using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hellhole.XUnitTest
{
    public class ParticipationsApiControllerTests
    {
        [Fact]
        public async Task TestGetParticipations()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsApiController tested = new ParticipationsApiController(dbContextMock.Object);

            // Act
            IEnumerable<Participation> actionResult = tested.GetParticipations();

            // Assert
            Assert.Null(actionResult);
        }

        [Fact]
        public async Task TestGetParticipation()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsApiController tested = new ParticipationsApiController(dbContextMock.Object);

            // Act
            IActionResult actionResult = await tested.GetParticipation(0, 0);

            // Assert
            Assert.Null(actionResult);
        }

    }
}
