using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;

namespace Hellhole.XUnitTest
{
    public class ParticipationsControllerTests
    {
        [Fact]
        public async void TestIndex()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            Mock<IHostingEnvironment> environmentMock = new Mock<IHostingEnvironment>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            IActionResult result = await tested.Index();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestDetails()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            IActionResult result = await tested.Details(0, 0);
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public void TestCreate()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            IActionResult result = tested.Create();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestCreateWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            Participation toAdd = new Participation();
            IActionResult result = await tested.Create(toAdd);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async void TestEdit()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            IActionResult result = await tested.Edit(0, 0);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async void TestEditWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            Participation toAdd = new Participation();
            toAdd.Canceled = false;
            IActionResult result = await tested.Create(toAdd);
            toAdd.Canceled = true;
            result = await tested.Edit(0, 0, toAdd);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public async void TestDelete()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            Participation toAdd = new Participation();
            await tested.Create(toAdd);
            IActionResult result = await tested.Delete(0, 0);

            // Assert
            Assert.IsType<ViewResult>(result);
        }


        [Fact]
        public async void TestDeleteConfirmed()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            ParticipationsController tested = new ParticipationsController(dbContextMock.Object);

            // Act
            Participation toAdd = new Participation();
            await tested.Create(toAdd);
            IActionResult result = await tested.DeleteConfirmed(0, 0);

            // Assert
            Assert.IsType<ViewResult>(result);
        }

    }
}
