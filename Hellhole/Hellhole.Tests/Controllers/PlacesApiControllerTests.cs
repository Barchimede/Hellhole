using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hellhole.XUnitTest
{
    public class PlacesApiControllerTests
    {
        [Fact]
        public async Task TestGetPlaces()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesApiController tested = new PlacesApiController(dbContextMock.Object);

            // Act
            IEnumerable<Place> actionResult = tested.GetPlaces();

            // Assert
            Assert.Null(actionResult);
        }

        [Fact]
        public async Task TestGetPlace()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesApiController tested = new PlacesApiController(dbContextMock.Object);

            // Act
            IActionResult actionResult = await tested.GetPlace(0);

            // Assert
            Assert.Null(actionResult);
        }

    }
}
