using Xunit;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;

namespace Hellhole.XUnitTest
{
    public class PlacesControllerTests
    {
        [Fact]
        public async void TestIndex()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesController tested = new PlacesController(dbContextMock.Object);

            // Act
            IActionResult result = await tested.Index();
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public void TestCreate()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesController tested = new PlacesController(dbContextMock.Object);

            // Act
            Place toAdd = new Place();
            IActionResult result = tested.Create();
            var viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestCreateWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesController tested = new PlacesController(dbContextMock.Object);

            // Act
            Place toAdd = new Place
            {
                Street="Avenue Benjamin Jansen",
                Number="23",
                Commune="Auderghem",
                City="Bruxelles",
                PostalCode="1160",
            };
            IActionResult result = await tested.Create(toAdd);

            // Assert
            Assert.IsType<RedirectToActionResult>(result);
        }

        [Fact]
        public async void TestDetails()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesController tested = new PlacesController(dbContextMock.Object);

            // Act
            IActionResult result = await tested.Details(0);
            ViewResult viewResult = Assert.IsType<ViewResult>(result);

            // Assert
            Assert.IsType<ViewResult>(viewResult);
        }

        [Fact]
        public async void TestEdit()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesController tested = new PlacesController(dbContextMock.Object);

            // Act
            Place toAdd = new Place();
            toAdd.Commune = "AAA";
            await tested.Create(toAdd);
            toAdd.Commune = "BBB";
            // IActionResult result = await tested.Edit(0, toAdd); // TOCHECK

            // Assert
            // Assert.IsType<IActionResult>(result); // TOCHECK
        }

        [Fact]
        public async void TestEditWithParameters()
        {
            // Arrange
            Mock<HellholeDbContext> dbContextMock = new Mock<HellholeDbContext>();
            PlacesController tested = new PlacesController(dbContextMock.Object);

            // Act
            Place toAdd = new Place();
            toAdd.Commune = "AAA";
            await tested.Create(toAdd);
            toAdd.Commune = "BBB";
            // IActionResult result = await tested.Edit(0, toAdd); // TOCHECK

            // Assert
            // Assert.IsType<ViewResult>(result); // TOCHECK
        }

    }
}
