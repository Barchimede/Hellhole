using Xunit;
using Hellhole.WebUI.Data;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.XUnitTest
{
    public class BandTests
    {
        public const int OTHER_VALIDATIONS_TO_IGNORE = 5;

        [Fact]
        public void TestBand()
        {
            // Arrange
            Band created;

            created = new Band();

            // Assert
            Assert.NotNull(created);
        }

        [Fact]
        public void TestBandID()
        {
            // Arrange
            Band created;
            int idCreation, idChanged;
            int newValue = 45;
            
            created = new Band();
            idCreation = created.BandID;

            created.BandID = newValue;
            idChanged = created.BandID;

            // Assert
            Assert.Equal(0, idCreation);
            Assert.Equal(newValue, idChanged);
        }

        [Fact]
        public void TestNameEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string name = "";

            // Act
            Band toValidate = new Band() { Name = name };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestNameWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string name = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Band toValidate = new Band() { Name = name };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validName = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validName);
        }

        [Fact]
        public void TestNameCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string name = "aaaaaaaaaaaaaaaaaaa";

            // Act
            Band toValidate = new Band() { Name = name };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validName = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validName);
        }

        [Fact]
        public void TestStyleEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string style = "";

            // Act
            Band toValidate = new Band() { Style = style };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStyle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validStyle);
        }

        [Fact]
        public void TestStyleWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string style = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Band toValidate = new Band() { Style = style };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStyle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validStyle);
        }

        [Fact]
        public void TestStyleCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string style = "aaaaaaaaaaaaaaaaaaa";

            // Act
            Band toValidate = new Band() { Style = style };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStyle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validStyle);
        }

        [Fact]
        public void TestOriginEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string origin = "";

            // Act
            Band toValidate = new Band() { Origin = origin };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validOrigin = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validOrigin);
        }

        [Fact]
        public void TestOriginWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string origin = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Band toValidate = new Band() { Origin = origin };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validOrigin = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validOrigin);
        }

        [Fact]
        public void TestOriginCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string origin = "AA";

            // Act
            Band toValidate = new Band() { Origin = origin };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validOrigin = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validOrigin);
        }

        [Fact]
        public void TestLinkSpotify()
        {
            // Arrange
            Band created;
            string linkCreation, linkChangedCorrectly;
            string newLinkCorrect = "aaa";

            created = new Band();
            linkCreation = created.LinkSpotify;

            created.LinkSpotify = newLinkCorrect;
            linkChangedCorrectly = created.LinkSpotify;

            // Assert
            Assert.Null(linkCreation);
            Assert.Equal(newLinkCorrect, linkChangedCorrectly);
        }

        [Fact]
        public void TestLinkYoutube()
        {
            // Arrange
            Band created;
            string linkCreation, linkChangedCorrectly;
            string newLinkCorrect = "aaa";

            created = new Band();
            linkCreation = created.LinkYoutube;

            created.LinkYoutube = newLinkCorrect;
            linkChangedCorrectly = created.LinkYoutube;

            // Assert
            Assert.Null(linkCreation);
            Assert.Equal(newLinkCorrect, linkChangedCorrectly);
        }

        [Fact]
        public void TestLinkBandcamp()
        {
            // Arrange
            Band created;
            string linkCreation, linkChangedCorrectly;
            string newLinkCorrect = "aaa";

            created = new Band();
            linkCreation = created.LinkBandcamp;

            created.LinkBandcamp = newLinkCorrect;
            linkChangedCorrectly = created.LinkBandcamp;

            // Assert
            Assert.Null(linkCreation);
            Assert.Equal(newLinkCorrect, linkChangedCorrectly);
        }

        [Fact]
        public void TestLinkSoundCloud()
        {
            // Arrange
            Band created;
            string linkCreation, linkChangedCorrectly;
            string newLinkCorrect = "aaa";

            created = new Band();
            linkCreation = created.LinkSoundCloud;

            created.LinkSoundCloud = newLinkCorrect;
            linkChangedCorrectly = created.LinkSoundCloud;

            // Assert
            Assert.Null(linkCreation);
            Assert.Equal(newLinkCorrect, linkChangedCorrectly);
        }

        [Fact]
        public void TestLinkImageEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkImage = "";

            // Act
            Band toValidate = new Band() { LinkImage = linkImage };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkImage = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validLinkImage);
        }

        [Fact]
        public void TestLinkImageWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkImage = null;

            // Act
            Band toValidate = new Band() { LinkImage = linkImage };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkImage = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validLinkImage);
        }

        [Fact]
        public void TestLinkImageCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkImage = "aaa";

            // Act
            Band toValidate = new Band() { LinkImage = linkImage };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkImage = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validLinkImage);
        }

        [Fact]
        public void TestLinkFacebookEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkFacebook = "";

            // Act
            Band toValidate = new Band() { LinkFacebook = linkFacebook };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkFacebook = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validLinkFacebook);
        }

        [Fact]
        public void TestLinkFacebookWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkFacebook = null;

            // Act
            Band toValidate = new Band() { LinkFacebook = linkFacebook };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkFacebook = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validLinkFacebook);
        }

        [Fact]
        public void TestLinkFacebookCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkFacebook = "aaa";

            // Act
            Band toValidate = new Band() { LinkFacebook = linkFacebook };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkFacebook = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validLinkFacebook);
        }

        [Fact]
        public void TestDescriptionEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string description = "";

            // Act
            Band toValidate = new Band() { Description = description };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDescription = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDescription);
        }

        [Fact]
        public void TestDescriptionWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string description = null;

            // Act
            Band toValidate = new Band() { Description = description };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDescription = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDescription);
        }

        [Fact]
        public void TestDescriptionCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string description = "aaa";

            // Act
            Band toValidate = new Band() { Description = description };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDescription = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validDescription);
        }

        [Fact]
        public void TestParticipations()
        {
            // Arrange
            Band created;
            ICollection<Participation> participationsCreation, participationsChangedCorrectly;
            ICollection<Participation> newParticipationsCorrect = new HashSet<Participation>();

            created = new Band();
            participationsCreation = created.Participations;

            created.Participations = newParticipationsCorrect;
            participationsChangedCorrectly = created.Participations;

            // Assert
            Assert.NotNull(participationsCreation);
            Assert.Equal(newParticipationsCorrect, participationsChangedCorrectly);
        }

    }
}
