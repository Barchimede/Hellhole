using Xunit;
using Hellhole.WebUI.Data;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.XUnitTest
{
    public class EventTests
    {
        public const int OTHER_VALIDATIONS_TO_IGNORE = 6;
        public const int OTHER_VALIDATIONS_TO_IGNORE_2 = 5;

        [Fact]
        public void TestEvent()
        {
            // Arrange
            Event created;

            created = new Event();

            // Assert
            Assert.NotNull(created);
        }

        [Fact]
        public void TestEventID()
        {
            // Arrange
            Event created;
            int idCreation, idChanged;
            int newValue = 45;

            created = new Event();
            idCreation = created.EventID;

            created.EventID = newValue;
            idChanged = created.EventID;

            // Assert
            Assert.Equal(0, idCreation);
            Assert.Equal(newValue, idChanged);
        }

        [Fact]
        public void TestDoorsEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            Event toValidate = new Event() { };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDoors = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validDoors);
        }

        [Fact]
        public void TestDoorsCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            DateTime doors = DateTime.Now.AddDays(1);
            DateTime curfew = DateTime.Now.AddDays(2);

            // Act
            Event toValidate = new Event() { Doors = doors, Curfew = curfew };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDoors = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validDoors);
        }

        [Fact]
        public void TestCurfew()
        {
            // Arrange
            Event created;
            DateTime curfewCreation, curfewChangedCorrectly;
            DateTime newCurfewCorrect = DateTime.MaxValue;

            created = new Event();
            curfewCreation = created.Curfew;

            created.Curfew = newCurfewCorrect;
            curfewChangedCorrectly = created.Curfew;

            // Assert
            Assert.IsType<DateTime>(curfewCreation);
            Assert.Equal(newCurfewCorrect, curfewChangedCorrectly);
        }

        [Fact]
        public void TestTitleEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string title = "";

            // Act
            Event toValidate = new Event() { Title = title };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validTitle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validTitle);
        }

        [Fact]
        public void TestTitleWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string title = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { Title = title };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validTitle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validTitle);
        }

        [Fact]
        public void TestTitleCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string title = "aaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { Title = title };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validTitle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validTitle);
        }

        [Fact]
        public void TestPriceZero()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            decimal price = 0;

            // Act
            Event toValidate = new Event() { Price = price };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validPrice = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validPrice);
        }

        [Fact]
        public void TestPriceNegative()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            decimal price = -1;

            // Act
            Event toValidate = new Event() { Price = price };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validPrice = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validPrice);
        }

        [Fact]
        public void TestPriceCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            decimal price = 1;

            // Act
            Event toValidate = new Event() { Price = price };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validPrice = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validPrice);
        }

        [Fact]
        public void TestTypeEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string type = "";

            // Act
            Event toValidate = new Event() { Type = type };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validType = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validType);
        }

        [Fact]
        public void TestTypeWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string type = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { Type = type };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validType = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validType);
        }

        [Fact]
        public void TestTypeCorrectParty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string type = "Party";

            // Act
            Event toValidate = new Event() { Type = type };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validType = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validType);
        }

        [Fact]
        public void TestTypeCorrectConcerty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string type = "Concert";

            // Act
            Event toValidate = new Event() { Type = type };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validType = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validType);
        }

        [Fact]
        public void TestStyleEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string style = "";

            // Act
            Event toValidate = new Event() { Style = style };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStyle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validStyle);
        }

        [Fact]
        public void TestStyleWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string style = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { Style = style };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStyle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validStyle);
        }

        [Fact]
        public void TestStyleCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string style = "aaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { Style = style };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStyle = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validStyle);
        }

        [Fact]
        public void TestDescriptionEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string description = null;

            // Act
            Event toValidate = new Event() { Description = description };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDescription = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validDescription);
        }

        [Fact]
        public void TestDescriptionCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string description = "aaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { Description = description };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDescription = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validDescription);
        }

        [Fact]
        public void TestLinkImageEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkImage = null;

            // Act
            Event toValidate = new Event() { LinkImage = linkImage };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkImage = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validLinkImage);
        }

        [Fact]
        public void TestLinkImageCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string linkImage = "aaaaaaaaaaaaaaaaaaa";

            // Act
            Event toValidate = new Event() { LinkImage = linkImage };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validLinkImage = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validLinkImage);
        }

        [Fact]
        public void TestCanceledFalse()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            bool canceled = false;

            // Act
            Event toValidate = new Event() { Canceled = canceled };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCanceled = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validCanceled);
        }

        [Fact]
        public void TestCanceledTrue()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            bool canceled = true;

            // Act
            Event toValidate = new Event() { Canceled = canceled };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCanceled = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.False(validCanceled);
        }

        [Fact]
        public void TestPlaceID()
        {
            // Arrange
            Event created;
            int idCreation, idChanged;
            int newValue = 45;

            created = new Event();
            idCreation = created.PlaceID;

            created.PlaceID = newValue;
            idChanged = created.PlaceID;

            // Assert
            Assert.Equal(0, idCreation);
            Assert.Equal(newValue, idChanged);
        }

        [Fact]
        public void TestPlace()
        {
            // Arrange
            Event created;
            Place toAdd;
            Place added;

            created = new Event();
            toAdd = new Place();

            created.Place = toAdd;
            added = created.Place;

            // Assert
            Assert.NotNull(added);
        }

        [Fact]
        public void TestParticipations()
        {
            // Arrange
            Event created;
            ICollection<Participation> participationsCreation, participationsChangedCorrectly;
            ICollection<Participation> newParticipationsCorrect = new HashSet<Participation>();

            created = new Event();
            participationsCreation = created.Participations;

            created.Participations = newParticipationsCorrect;
            participationsChangedCorrectly = created.Participations;

            // Assert
            Assert.NotNull(participationsCreation);
            Assert.Equal(newParticipationsCorrect, participationsChangedCorrectly);
        }

    }
}
