using Xunit;
using Hellhole.WebUI.Data;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Hellhole.XUnitTest
{
    public class ParticipationTests
    {
        public const int OTHER_VALIDATIONS_TO_IGNORE = 1;

        [Fact]
        public void TestParticipation()
        {
            // Arrange
            Participation created;

            created = new Participation();

            // Assert
            Assert.NotNull(created);
        }

        [Fact]
        public void TestEventIDEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            Participation toValidate = new Participation() { };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validEventID = (validationResults.Count) == 0;

            //Assert
            Assert.False(validEventID);
        }

        [Fact]
        public void TestEventIDValid()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            int eventID = 0;

            // Act
            Participation toValidate = new Participation() { EventID = eventID };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validEventID = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validEventID);
        }

        [Fact]
        public void TestBandIDEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            Participation toValidate = new Participation() { };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validBandID = (validationResults.Count) == 0;

            //Assert
            Assert.False(validBandID);
        }

        [Fact]
        public void TestBandIDValid()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            int bandID = 0;

            // Act
            Participation toValidate = new Participation() { BandID = bandID };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validBandID = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validBandID);
        }

        [Fact]
        public void TestStarting()
        {
            // Arrange
            Participation created;
            DateTime startingCreation, startingChangedWrongly, startingChangedCorrectly;
            DateTime newStartingWrong = DateTime.Now, newStartingCorrect = DateTime.MaxValue;

            created = new Participation();
            startingCreation = created.Starting;

            created.Starting = newStartingWrong;
            startingChangedWrongly = created.Starting;

            created.Starting = newStartingCorrect;
            startingChangedCorrectly = created.Starting;

            // Assert
            Assert.IsType<DateTime>(startingCreation);
            Assert.NotEqual(startingCreation, startingChangedWrongly);
            Assert.Equal(newStartingCorrect, startingChangedCorrectly);
        }

        [Fact]
        public void TestEnding()
        {
            // Arrange
            Participation created;
            DateTime endingCreation, endingChangedWrongly, endingChangedCorrectly;
            DateTime newEndingWrong = DateTime.Now, newEndingCorrect = DateTime.MaxValue;

            created = new Participation();
            endingCreation = created.Ending;

            created.Ending = newEndingWrong;
            endingChangedWrongly = created.Ending;

            created.Ending = newEndingCorrect;
            endingChangedCorrectly = created.Ending;

            // Assert
            Assert.IsType<DateTime>(endingCreation);
            Assert.NotEqual(endingCreation, endingChangedWrongly);
            Assert.Equal(newEndingCorrect, endingChangedCorrectly);
        }

        [Fact]
        public void TestCanceledEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            Participation toValidate = new Participation() { };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCanceled = (validationResults.Count) == 0;

            //Assert
            Assert.False(validCanceled);
        }

        [Fact]
        public void TestCanceledValid()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            bool canceled = false;

            // Act
            Participation toValidate = new Participation() { Canceled = canceled };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCanceled = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validCanceled);
        }

        [Fact]
        public void TestBand()
        {
            // Arrange
            Participation created;
            Band bandCreation, bandChangedCorrectly;
            Band newBandCorrect = new Band();

            created = new Participation();
            bandCreation = created.Band;

            created.Band = newBandCorrect;
            bandChangedCorrectly = created.Band;

            // Assert
            Assert.Null(bandCreation);
            Assert.Equal(newBandCorrect, bandChangedCorrectly);
        }

        [Fact]
        public void TestEvent()
        {
            // Arrange
            Participation created;
            Event eventCreation, eventChangedCorrectly;
            Event newEventCorrect = new Event();

            created = new Participation();
            eventCreation = created.Event;

            created.Event = newEventCorrect;
            eventChangedCorrectly = created.Event;

            // Assert
            Assert.Null(eventCreation);
            Assert.Equal(newEventCorrect, eventChangedCorrectly);
        }

    }
}
