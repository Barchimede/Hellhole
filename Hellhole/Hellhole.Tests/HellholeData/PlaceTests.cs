using Xunit;
using Hellhole.WebUI.Data;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Hellhole.XUnitTest
{
    public class PlaceTests
    {
        public const int OTHER_VALIDATIONS_TO_IGNORE = 4;
        public const int OTHER_VALIDATIONS_TO_IGNORE_2 = 5;

        [Fact]
        public void TestPlace()
        {
            // Arrange
            Place created;

            created = new Place();

            // Assert
            Assert.NotNull(created);
        }

        [Fact]
        public void TestPlaceID()
        {
            // Arrange
            Place created;
            int idCreation, idChanged;
            int newValue = 45;

            created = new Place();
            idCreation = created.PlaceID;

            created.PlaceID = newValue;
            idChanged = created.PlaceID;

            // Assert
            Assert.Equal(0, idCreation);
            Assert.Equal(newValue, idChanged);
        }

        [Fact]
        public void TestPostalCodeEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string postalCode = "";

            // Act
            Place toValidate = new Place() { PostalCode = postalCode };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestPostalCodeWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string postalCode = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Place toValidate = new Place() { PostalCode = postalCode };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validPostalCode = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validPostalCode);
        }

        [Fact]
        public void TestPostalCodeCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string postalCode = "1111";

            // Act
            Place toValidate = new Place() { PostalCode = postalCode };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validPostalCode = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validPostalCode);
        }

        [Fact]
        public void TestCityEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string city = "";

            // Act
            Place toValidate = new Place() { City = city };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestCityWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string city = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Place toValidate = new Place() { City = city };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCity = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validCity);
        }

        [Fact]
        public void TestCityCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string city = "aaa";

            // Act
            Place toValidate = new Place() { City = city };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCity = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validCity);
        }

        [Fact]
        public void TestStreetEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string street = "";

            // Act
            Place toValidate = new Place() { Street = street };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestStreetWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string street = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Place toValidate = new Place() { Street = street };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStreet = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validStreet);
        }

        [Fact]
        public void TestStreetCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string street = "aaa";

            // Act
            Place toValidate = new Place() { Street = street };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validStreet = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validStreet);
        }

        [Fact]
        public void TestNumberEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string number = "";

            // Act
            Place toValidate = new Place() { Number = number };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestNumberWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string number = "1111111111111111111111111";

            // Act
            Place toValidate = new Place() { Number = number };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validNumber = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validNumber);
        }

        [Fact]
        public void TestNumberCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string number = "1";

            // Act
            Place toValidate = new Place() { Number = number };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validNumber = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validNumber);
        }

        [Fact]
        public void TestCommuneEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string commune = "";

            // Act
            Place toValidate = new Place() { Commune = commune };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestCommuneWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string commune = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Place toValidate = new Place() { Commune = commune };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCommune = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validCommune);
        }

        [Fact]
        public void TestCommuneCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string commune = "aaa";

            // Act
            Place toValidate = new Place() { Commune = commune };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validCommune = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.True(validCommune);
        }

        [Fact]
        public void TestMailboxEmpty()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string mailbox = "";

            // Act
            Place toValidate = new Place() { Mailbox = mailbox };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestMailboxWrong()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string mailbox = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            // Act
            Place toValidate = new Place() { Mailbox = mailbox };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validMailbox = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            //Assert
            Assert.False(validMailbox);
        }

        [Fact]
        public void TestMailboxCorrect()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();
            string mailbox = "1";

            // Act
            Place toValidate = new Place() { Mailbox = mailbox };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validMailbox = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE_2) == 0;

            //Assert
            Assert.True(validMailbox);
        }

        [Fact]
        public void TestEvents()
        {
            // Arrange
            Place created;
            ICollection<Event> participationsCreation, participationsChangedCorrectly;
            ICollection<Event> newEventsCorrect = new HashSet<Event>();

            created = new Place();
            participationsCreation = created.Events;

            created.Events = newEventsCorrect;
            participationsChangedCorrectly = created.Events;

            // Assert
            Assert.NotNull(participationsCreation);
            Assert.Equal(newEventsCorrect, participationsChangedCorrectly);
        }

        [Fact]
        public void TestToString()
        {
            // Arrange
            Place created;
            string correctOutput = "1, aaa bbb - 1111 ccc (Boite : 2)";

            created = new Place();
            created.Number = "1";
            created.Street = "aaa";
            created.City = "bbb";
            created.PostalCode = "1111";
            created.Commune = "ccc";
            created.Mailbox = "2";

            // Assert
            Assert.Equal(correctOutput, created.ToString());
        }

    }
}
