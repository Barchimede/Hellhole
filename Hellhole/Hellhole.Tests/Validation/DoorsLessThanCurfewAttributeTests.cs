using Xunit;
using Hellhole.WebUI.Data;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.XUnitTest
{
    public class DoorsLessThanCurfewAttributeTests
    {
        public const int OTHER_VALIDATIONS_TO_IGNORE = 5;

        [Fact]
        public void TestIsValidBeforeDate()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            DateTime before = new DateTime().AddDays(1);
            DateTime after = new DateTime().AddDays(2);
            Event toValidate = new Event() { Doors = before, Curfew = after };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            // Assert
            Assert.True(validDate);
        }

        [Fact]
        public void TestIsValidAfterDate()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            DateTime before = new DateTime().AddDays(1);
            DateTime after = new DateTime().AddDays(2);
            Event toValidate = new Event() { Doors = after, Curfew = before };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            // Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestIsValidSameDate()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            DateTime date = new DateTime().AddDays(1);
            Event toValidate = new Event() { Doors = date, Curfew = date };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = (validationResults.Count - OTHER_VALIDATIONS_TO_IGNORE) == 0;

            // Assert
            Assert.False(validDate);
        }
    }
}
