using Xunit;
using Hellhole.WebUI.Data;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.XUnitTest
{
    public class StartingLessThanEndingAttribute
    {
        [Fact]
        public void TestIsValidBeforeDate()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            DateTime before = new DateTime().AddDays(1);
            DateTime after = new DateTime().AddDays(2);
            Participation toValidate = new Participation() { Starting = before, Ending = after };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = validationResults.Count == 0;

            // Assert
            Assert.True(validDate);
        }

        [Fact]
        public void TestIsValidAfterDate()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            DateTime before = new DateTime().AddDays(1);
            DateTime after = new DateTime().AddDays(2);
            Participation toValidate = new Participation() { Starting = after, Ending = before };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = validationResults.Count == 0;

            // Assert
            Assert.False(validDate);
        }

        [Fact]
        public void TestIsValidSameDate()
        {
            // Arrange
            List<ValidationResult> validationResults = new List<ValidationResult>();

            // Act
            DateTime date = new DateTime().AddDays(1);
            Participation toValidate = new Participation() { Starting = date, Ending = date };
            ValidationContext context = new ValidationContext(toValidate);
            Validator.TryValidateObject(toValidate, context, validationResults, true);
            bool validDate = validationResults.Count == 0;

            // Assert
            Assert.False(validDate);
        }

    }
}
