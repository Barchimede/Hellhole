﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;

namespace Hellhole.WebUI.Controllers
{
    [Produces("application/json")]
    [Route("api/Bands")]
    public class BandsApiController : Controller
    {
        private readonly HellholeDbContext _context;

        public BandsApiController(HellholeDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get all bands 
        /// </summary>
        /// <returns>All bands</returns>
        // GET: api/Bands
        [HttpGet]
        public IEnumerable<Band> GetBands()
        {
            if (_context.Bands == null)
                return null;
            return _context.Bands.Include(band => band.Participations).ThenInclude(pa => pa.Event);
        }
        /// <summary>
        /// Get the band given the ID
        /// </summary>
        /// <param name="id">The BandID of the band to find</param>
        /// <returns>Ok ActionResult with its band or else NotFound ActionResult</returns>
        // GET: api/Bands/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBand([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_context.Bands == null)
                return null;

            var band = await _context.Bands
                .Include(ba => ba.Participations)
                    .ThenInclude(pa => pa.Event)
                .SingleOrDefaultAsync(ba => ba.BandID == id);
            if (band == null)
            {
                return NotFound();
            }

            return Ok(band);
        }

        /// <summary>
        /// Get all the bands sorted by name
        /// </summary>
        /// <returns>All the bands in the database sorted by name (ascending)</returns>
        [Route("SortedByName")]
        public IEnumerable<Band> GetBandsSortedByName()
        {
            return _context.Bands.OrderBy(band => band.Name);
        }

        /// <summary>
        /// Get the band with the specified id
        /// </summary>
        /// <param name="id">The BandID of the band needed</param>
        /// <returns>The band found with the id</returns>
        private async Task<Band> getBandByIdAsync(int id)
        {
            Band b = await _context.Bands.SingleOrDefaultAsync(m => m.BandID == id);

            return b;
        }

        /// <summary>
        /// Check whether the band exists or not
        /// </summary>
        /// <param name="id">The BandID of the band needed</param>
        /// <returns>true if the event exists or else false</returns>
        private bool BandExists(int id)
        {
            return _context.Bands.Any(e => e.BandID == id);
        }
    }
}