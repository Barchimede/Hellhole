﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;
using Microsoft.AspNetCore.Http;
using System.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace Hellhole.WebUI.Controllers
{
    [Authorize]
    public class BandsController : Controller
    {
        private readonly HellholeDbContext _context;
        private IHostingEnvironment _environment;

        private const string picDirectory = "Content\\Bands";

        public BandsController(HellholeDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        /// <summary>
        /// GET the index view of bands
        /// </summary>
        /// <returns>The Index view of all bands</returns>
        // GET: Bands
        public async Task<IActionResult> Index()
        {
            if (_context.Bands == null)
                return View();
            return View(await _context.Bands.ToListAsync());
        }

        /// <summary>
        /// GET the details view of a band with its ID
        /// </summary>
        /// <param name="id">The BandID of the band</param>
        /// <returns>The details view of the band</returns>
        // GET: Bands/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (_context.Bands == null)
                return View();

            if (id == null)
            {
                return NotFound();
            }

            var band = await _context.Bands
                .SingleOrDefaultAsync(m => m.BandID == id);
            if (band == null)
            {
                return NotFound();
            }

            return View(band);
        }

        /// <summary>
        /// GET the view to create a new band
        /// </summary>
        /// <returns>The view to create a new band</returns>
        // GET: Bands/Create
        public IActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// Creates a new band and redirect to index
        /// </summary>
        /// <param name="band">The band to create</param>
        /// <param name="image">The image file to insert in WebRoot</param>
        /// <returns>The index vieuw if succeeded or else the create view with validation errors</returns>
        // POST: Bands/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BandID,Name,Style,LinkImage,Origin,LinkSpotify,LinkSoundCloud,LinkFacebook, LinkYoutube, LinkBandcamp ,Description")] Band band, IFormFile image)
        {
            if (ModelState.IsValid)
            {
                if (image == null || image.Length == 0)
                {
                    ModelState.AddModelError(string.Empty, "Image not uploaded");
                    return View(band);
                }
                await this.UploadImageToBandAsync(image, band);

                _context.Add(band);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(band);
        }

        /// <summary>
        /// Get the view to edit a band
        /// </summary>
        /// <param name="id">The id of the band to edit</param>
        /// <returns>The view to edit a band</returns>
        // GET: Bands/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (_context.Bands == null)
                return View();

            if (id == null)
            {
                return NotFound();
            }

            var band = await _context.Bands.SingleOrDefaultAsync(m => m.BandID == id);
            if (band == null)
            {
                return NotFound();
            }
            return View(band);
        }

        /// <summary>
        /// Edits the band with its ID
        /// </summary>
        /// <param name="id">The ID of the band</param>
        /// <param name="band">The band to edit</param>
        /// <param name="file">The file to insert in webRoot</param>
        /// <returns>The index view if succeeded or else the edit view with validation errors</returns>
        // POST: Bands/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BandID,Name,Style,LinkImage,Origin,LinkSpotify,LinkSoundCloud,LinkFacebook, LinkYoutube, LinkBandcamp ,Description")] Band band, IFormFile file = null)
        {
            if (id != band.BandID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    //File upload
                    if (file != null && file.Length != 0)
                    {
                        string filename = _environment.WebRootPath + band.LinkImage,
                            formerExtension = Path.GetExtension(band.LinkImage),
                            nextExtension = Path.GetExtension(file.FileName),
                            newFilename = filename;

                        if (!formerExtension.Equals(nextExtension))
                        {
                            newFilename = Path.Combine(_environment.WebRootPath, picDirectory, Path.GetFileNameWithoutExtension(filename) + nextExtension);
                            System.IO.File.Move(filename, newFilename);
                        }
                        //string newFilename = Path.GetFileNameWithoutExtension(filename) + Path.GetExtension(file.FileName);

                        using (var fileStream = new FileStream(newFilename, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            band.LinkImage = "\\" + picDirectory + "\\" + Path.GetFileNameWithoutExtension(band.LinkImage) + nextExtension;
                        }
                    }
                    _context.Update(band);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BandExists(band.BandID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(band);
        }

        /// <summary>
        /// Uploads a file to webRoot and replace if already exists
        /// </summary>
        /// <param name="file">The file to upload</param>
        /// <param name="band">The band thats owns the file</param>
        /// <returns></returns>
        private async Task UploadImageToBandAsync(IFormFile file, Band band)
        {
            string extension = Path.GetExtension(file.FileName);
            string filename = Path.Combine(picDirectory, Guid.NewGuid().ToString() + extension);
            using (var fileStream = new FileStream(Path.Combine(_environment.WebRootPath, filename), FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
                band.LinkImage = "\\" + filename;
            }
        }
        /// <summary>
        /// Checks whether a band exsists or not
        /// </summary>
        /// <param name="id">The id of the band to check</param>
        /// <returns>True if the band exists or else false</returns>
        private bool BandExists(int id)
        {
            return _context.Bands.Any(e => e.BandID == id);
        }
    }
}
