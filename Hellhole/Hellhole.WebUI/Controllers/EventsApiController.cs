﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;
using Hellhole.WebUI.Models;

namespace Hellhole.WebUI.Controllers
{
    [Produces("application/json")]
    [Route("api/Events")]
    public class EventsApiController : Controller
    {

        private readonly HellholeDbContext _context;

        public EventsApiController(HellholeDbContext context)
        {
            _context = context;
        }

        // GET: api/Events

        /// <summary>
        /// Get all the events 
        /// </summary>
        /// <returns>All the events in the database</returns>
        [HttpGet]
        public IEnumerable<Event> GetEvents()
        {
            if (_context.Events == null)
                return null;
            return _context.Events.Include(e => e.Participations).Include(e => e.Place);
        }

        // GET: api/Events/5

        /// <summary>
        /// Get the event with the given ID
        /// </summary>
        /// <param name="id">The id of the event required</param>
        /// <returns>The event found or else a BadRequest</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEvent([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_context.Events == null)
                return null;

            var @event = await _context.Events
                .Include(e => e.Participations)
                    .ThenInclude(pa => pa.Band)
                .Include(e => e.Place)
                .SingleOrDefaultAsync(m => m.EventID == id);

            if (@event == null)
            {
                return NotFound();
            }

            return Ok(@event);
        }
        
        /// <summary>
        /// API Method that sends the list of the next events compared to now in an ascending way
        /// </summary>
        /// <returns>the list of next events order by the curfew date (ascending) </returns>
        [Route("NextOrderByCurfewAscending")]
        [HttpGet]
        public EventsList GetNextEventListAscending()
        {
            if (_context.Events == null)
                return null;
            IEnumerable<Event> events = _context.Events
                .Include(ev => ev.Place)
                .Where(ev => ev.Curfew.CompareTo(DateTime.Now) > 0).OrderBy(ev => ev.Curfew);
            return CreateEventList(events);
        }

        /// <summary>
        ///  API Method that sends the list of the previous non-canceled events compared to now in a descending way
        /// </summary>
        /// <returns>The list of previous non-canceled events ordered by the curfew date (descending) </returns>
        [Route("PreviousOrderByCurfewDescendingNotCanceled")]
        [HttpGet]
        public EventsList GetPreviousEventListDescending()
        {
            if (_context.Events == null)
                return null;
            IEnumerable<Event> events = _context.Events
                .Include(ev => ev.Place)
                .Where(ev => ev.Curfew.CompareTo(DateTime.Now) <= 0 && !ev.Canceled).OrderByDescending(ev => ev.Curfew);
            return CreateEventList(events);
        }

        /// <summary>
        /// Create an EventsList according to the list of events in param
        /// </summary>
        /// <param name="events">The list of events used to create the EventsList</param>
        /// <returns>The EventsList created</returns>
        private EventsList CreateEventList(IEnumerable<Event> events)
        {
            return new EventsList
            {
                All = events,
                Concerts = events.Where(ev => ev.Type.Equals(Event.Concert)),
                Parties = events.Where(ev => ev.Type.Equals(Event.Party))
            };
        }

        /// <summary>
        /// Check in database whether an Event exists
        /// </summary>
        /// <param name="id">The id of the checked event</param>
        /// <returns>true if event exists or else false</returns>
        private bool EventExists(int id)
        {
            return _context.Events.Any(e => e.EventID == id);
        }
    }
}