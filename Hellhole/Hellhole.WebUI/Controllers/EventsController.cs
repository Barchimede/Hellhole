﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using Hellhole.WebUI.Models;

namespace Hellhole.WebUI.Controllers 
{
    [Authorize]
    public class EventsController : Controller
    {
        private readonly HellholeDbContext _context;
        private readonly IHostingEnvironment _environment;

        private const string picDirectory = "Content\\Events";

        public EventsController(HellholeDbContext context, IHostingEnvironment environment)
        {
            _environment = environment;
            _context = context;
        }

        // GET: Events
        public async Task<IActionResult> Index()
        {
            if (_context.Events == null)
                return View();
            var hellholeDbContext = _context.Events.Include(e => e.Place);
            return View(await hellholeDbContext.ToListAsync());
        }

        // GET: Events/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (_context.Events == null)
                return View();

            if (id == null)
            {
                return NotFound();
            }

            var @event = await _context.Events
                .Include(e => e.Place)
                .SingleOrDefaultAsync(m => m.EventID == id);
            if (@event == null)
            {
                return NotFound();
            }

            return View(@event);
        }

        // GET: Events/Create
        public IActionResult Create()
        {
            if (_context.Events == null)
                return View();

            ViewData["PlaceID"] = new SelectList(_context.Places, "PlaceID", "Street");
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("EventID,Doors,Curfew,Title,Price,Type,Style,Description,LinkImage,Canceled,PlaceID")] Event @event, IFormFile image = null)
        {
            if (_context.Events == null)
                return View();

            if (ModelState.IsValid)
            {
                if (image == null || image.Length == 0)
                {
                    ModelState.AddModelError(string.Empty, "Image not uploaded");
                    ViewData["PlaceID"] = new SelectList(_context.Places, "PlaceID", "Street", @event.PlaceID);
                    return View(@event);
                }
                await this.UploadImageToBandAsync(image, @event);
                _context.Add(@event);
                await _context.SaveChangesAsync();
                PushNotification(@event);
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlaceID"] = new SelectList(_context.Places, "PlaceID", "Street", @event.PlaceID);
            return View(@event);
        }

        // GET: Events/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (_context.Events == null)
                return View();

            if (id == null)
            {
                return NotFound();
            }

            Event @event = await _context.Events.SingleOrDefaultAsync(m => m.EventID == id);
            ViewBag.OtherType = Event.Concert;
            if (@event.Type.Equals(Event.Concert)) ViewBag.OtherType = Event.Party;

            if (@event == null)
            {
                return NotFound();
            }
            ViewData["PlaceID"] = new SelectList(_context.Places, "PlaceID", "Street", @event.PlaceID);
            return View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("EventID,Doors,Curfew,Title,Price,Type,Style,Description,LinkImage,Canceled,PlaceID")] Event @event, IFormFile file = null)
        {
            if (_context.Events == null)
                return View();

            if (id != @event.EventID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //File upload
                    if (file != null && file.Length != 0)
                    {
                        string filename = _environment.WebRootPath + @event.LinkImage,
                            formerExtension = Path.GetExtension(@event.LinkImage),
                            nextExtension = Path.GetExtension(file.FileName),
                            newFilename = filename;

                        if (!formerExtension.Equals(nextExtension))
                        {
                            newFilename = Path.Combine(_environment.WebRootPath, picDirectory, Path.GetFileNameWithoutExtension(filename) + nextExtension);
                            System.IO.File.Move(filename, newFilename);
                        }

                        using (var fileStream = new FileStream(newFilename, FileMode.Create))
                        {
                            await file.CopyToAsync(fileStream);
                            @event.LinkImage = "\\" + picDirectory + "\\" + Path.GetFileNameWithoutExtension(@event.LinkImage) + nextExtension;
                        }
                    }

                    _context.Update(@event);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.EventID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PlaceID"] = new SelectList(_context.Places, "PlaceID", "Street", @event.PlaceID);
            return View(@event);
        }

        /// <summary>
        /// Checks whether an event exists in the database
        /// </summary>
        /// <param name="id">The id of the event to find</param>
        /// <returns>true if the event exists or else false</returns>
        private bool EventExists(int id)
        {
            return _context.Events.Any(e => e.EventID == id);
        }

        /// <summary>
        /// Uploads the picture of the band and replace the current picture if it already exists
        /// </summary>
        /// <param name="file">The picture to upload</param>
        /// <param name="@event">The event needed</param>
        /// <returns></returns>
        private async Task UploadImageToBandAsync(IFormFile file, Event @event)
        {
            string extension = Path.GetExtension(file.FileName);
            string filename = Path.Combine(picDirectory, Guid.NewGuid().ToString() + extension);
            using (var fileStream = new FileStream(Path.Combine(_environment.WebRootPath, filename), FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
                @event.LinkImage = "\\" + filename;
            }
        }

        //Effectue une requete post à OneSignal 
        //OneSignal envoie une notification à tt les appareils

        /// <summary>
        /// Makes a post request to OneSignal. Onesignal will send a notification to all the smartphones
        /// </summary>
        /// <param name="@event"></param>
        private void PushNotification(Event @event)
        {
            HttpWebRequest request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;
            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            request.Headers.Add("authorization", "Basic YjFkNDY5NTQtNGQzMi00ODlkLWEyZmYtYjMwNzllNGJjNGI2");
            JsonSerializer serializer = new JsonSerializer();
            var obj = new
            {
                app_id = "792b1e1e-6a40-43c0-878e-f85d3be5b369",
                headings = new { en = "New Event !" },
                contents = new { en = "Hey ! The event \"" + @event.Title + "\" is upcoming, check the details now!" },
                data = new { id = @event.EventID },
                big_picture = "http://hellhole-admin.azurewebsites.net/" + @event.LinkImage,// à modifier
                included_segments = new string[] { "All" }
            };
            String param = JsonConvert.SerializeObject(obj);
            byte[] byteArray = Encoding.UTF8.GetBytes(param);
            string responseContent = null;
            try
            {
                using (Stream writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }
                using (WebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            }
            System.Diagnostics.Debug.WriteLine(responseContent);
        }

    }
}
