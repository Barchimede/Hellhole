﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Hellhole.WebUI.Models;
using Microsoft.AspNetCore.Authorization;

namespace Hellhole.WebUI.Controllers
{ 
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// Get the Index View
        /// </summary>
        /// <returns>The index view</returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get the About view
        /// </summary>
        /// <returns>The about view</returns>
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Gets the Contact page
        /// </summary>
        /// <returns>The contact view</returns>
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Gets the songs chat view in order to choose the song you want
        /// </summary>
        /// <returns>The songs chat view</returns>
        public IActionResult Songs()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Error()
        {
            if (Activity.Current == null)
                return null;
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
