﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;

namespace Hellhole.WebUI.Controllers
{
    [Produces("application/json")]
    [Route("api/Participations")]
    public class ParticipationsApiController : Controller
    {
        private readonly HellholeDbContext _context;

        public ParticipationsApiController(HellholeDbContext context)
        {
            _context = context;
        }
        // GET: api/Participations

        /// <summary>
        /// Get all the participations from the DB
        /// </summary>
        /// <returns>All the participations from the DB</returns>
        [HttpGet]
        public IEnumerable<Participation> GetParticipations()
        {
            if (_context.Events == null)
                return null;

            return _context.Participations
                .Include(pa => pa.Band)
                .Include(pa => pa.Event)
                .ThenInclude(e => e.Place);
        }

        // GET: api/Participations/5

        /// <summary>
        /// Get the participation with the given eventID and bandID
        /// </summary>
        /// <param name="eventid">The eventID of the participation</param>
        /// <param name="bandid">The bandID of the participation</param>
        /// <returns>The participation  found or else a bad request</returns>
        [HttpGet("{eventid}/{bandid}")]
        public async Task<IActionResult> GetParticipation([FromRoute] int eventid, int bandid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_context.Events == null)
                return null;

            Participation participation = await _context.Participations
                .Include(pa => pa.Band)
                .Include(pa => pa.Event)
                .ThenInclude(e => e.Place)
                .SingleOrDefaultAsync(m => m.EventID == eventid && m.BandID == bandid);

            if (participation == null)
            {
                return NotFound();
            }

            return Ok(participation);
        }
    }
}