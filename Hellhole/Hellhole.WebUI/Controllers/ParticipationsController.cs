﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;
using Microsoft.AspNetCore.Authorization;

namespace Hellhole.WebUI.Controllers
{
    [Authorize]
    public class ParticipationsController : Controller
    {
        private readonly HellholeDbContext _context;

        public ParticipationsController(HellholeDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Get the list of all participations in database
        /// </summary>
        /// <returns>The view that displays all the participations</returns>
        // GET: Participations
        public async Task<IActionResult> Index()
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            var hellholeDbContext = _context.Participations.Include(p => p.Band).Include(p => p.Event);
            return View(await hellholeDbContext.ToListAsync());
        }

        /// <summary>
        /// GET the detail of a participation with its given concatenated key thanks to Razor page
        /// </summary>
        /// <param name="eventid">The id of the event attached to the participation</param>
        /// <param name="bandid">The id of the event attached to the participation</param>
        /// <returns>The view that displays the detail of the participation</returns>
        // GET: Participations/Details/6/9
        [Route("/Participations/Details/{eventid?}/{bandid?}")]
        public async Task<IActionResult> Details(int? eventid, int? bandid)
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            if (eventid == null || bandid == null)
            {
                return NotFound();
            }

            var participation = await _context.Participations
                .Include(p => p.Band)
                .Include(p => p.Event)
                .SingleOrDefaultAsync(m => m.EventID == eventid && m.BandID == bandid);
            if (participation == null)
            {
                return NotFound();
            }
            
            return View(participation);
        }

        /// <summary>
        /// GET the view used to create a new participation
        /// </summary>
        /// <returns>The view to create a new participation</returns>
        // GET: Participations/Create
        public IActionResult Create()
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            ViewData["BandID"] = new SelectList(_context.Bands, "BandID", "Name");
            ViewData["EventID"] = new SelectList(_context.Events.Where(e => !e.Canceled), "EventID", "Title");
            return View();
        }

        /// <summary>
        /// Creates a new participation
        /// </summary>
        /// <param name="participation">the new participation to create</param>
        /// <returns>Redirection to index if succeeded or else the Create view with error messages </returns>
        // POST: Participations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create([Bind("EventID,BandID,Starting,Ending,Canceled")] Participation participation)
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            if (ModelState.IsValid)
            {
                Event ev = await _context.Events.SingleOrDefaultAsync(e => e.EventID == participation.EventID);
                if (ev == null || ev.Canceled)
                    ModelState.AddModelError(string.Empty, "You cannot create a participation to a canceled event.");
                if (ev.Type == "Party")
                    ModelState.AddModelError(string.Empty, "You cannot create a participation to a party.");
                if (participation.Starting.CompareTo(ev.Doors) < 0 || participation.Starting.CompareTo(ev.Curfew) > 0)
                    ModelState.AddModelError(string.Empty, "You cannot create a participation that starts before/after its event.");
                if (participation.Ending.CompareTo(ev.Curfew) > 0 || participation.Ending.CompareTo(ev.Doors) < 0)
                    ModelState.AddModelError(string.Empty, "You cannot create a participation that ends before/after its event's doors/curfew.");
                if (participation.Canceled)
                    ModelState.AddModelError(string.Empty, "You can't create a canceled participation.");
                if (ModelState.IsValid)
                {
                    _context.Add(participation);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["BandID"] = new SelectList(_context.Bands, "BandID", "Name", participation.BandID);
            ViewData["EventID"] = new SelectList(_context.Events.Where(e => !e.Canceled && e.Type.Equals("Concert")), "EventID", "Title", participation.EventID);
            return View(participation);
        }

        /// <summary>
        /// GET the view to edit the participaton with the concatenated key
        /// </summary>
        /// <param name="eventid">The EventID of the editable participation</param>
        /// <param name="bandid">The BandID of the editable participation</param>
        /// <returns>The view to edit the participation</returns>
        // GET: Participations/Edit/6/9
        [Route("/Participations/Edit/{eventid?}/{bandid?}")]
        public async Task<IActionResult> Edit(int? eventid, int? bandid)
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            if (eventid == null || bandid == null)
            {
                return NotFound();
            }

            var participation = await _context.Participations.SingleOrDefaultAsync(m => m.EventID == eventid && m.BandID == bandid);
            if (participation == null)
            { 
                return NotFound();
            }
            ViewData["BandID"] = new SelectList(_context.Bands, "BandID", "Name", participation.BandID);
            ViewData["EventID"] = new SelectList(_context.Events.Where(e => !e.Canceled && e.Type.Equals("Concert")), "EventID", "Title", participation.EventID);
            return View(participation);
        }

        /// <summary>
        /// Edit the participation and redirect to index if succeeded
        /// </summary>
        /// <param name="eventid">The EventID of the participation to edit</param>
        /// <param name="bandid">The BandID of the participation to edit</param>
        /// <param name="participation">The participation to edit </param>
        /// <returns>Redirect to the index if succeeded or else the Edit view with validation errors</returns>
        // POST: Participations/Edit/5/2
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Route("/Participations/Edit/{eventid}/{bandid}")]
        public async Task<IActionResult> Edit(int eventid, int bandid, [Bind("EventID,BandID,Starting,Ending,Canceled")] Participation participation)
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            if (eventid != participation.EventID && bandid != participation.BandID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                Event ev = await _context.Events.SingleOrDefaultAsync(e => e.EventID == participation.EventID);
                if (participation.Starting.CompareTo(ev.Doors) < 0)
                    ModelState.AddModelError("Starting", "You cannot modify a participation that starts before its event.");
                else if (participation.Ending.CompareTo(ev.Curfew) > 0)
                    ModelState.AddModelError("Ending", "You cannot modify a participation that ends after its event.");
                else
                {
                    try
                    {
                        _context.Update(participation);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!ParticipationExists(participation.EventID, participation.BandID))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                
            }
            ViewData["BandID"] = new SelectList(_context.Bands, "BandID", "Name", participation.BandID);
            ViewData["EventID"] = new SelectList(_context.Events.Where(e => !e.Canceled && e.Type.Equals("Concert")), "EventID", "Title", participation.EventID);
            return View(participation);
        }

        /// <summary>
        /// GET the view to delete the participation with concatenated key in parameters
        /// </summary>
        /// <param name="eventid">The EventID of the participation to delete</param>
        /// <param name="bandid">The BandID of the participation to delete</param>
        /// <returns>The view to delete the participation</returns>
        // GET: Participations/Delete/6/9
        [Route("/Participations/Delete/{eventid?}/{bandid?}")]
        [HttpGet]
        public async Task<IActionResult> Delete(int? eventid, int? bandid)
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            if (eventid == null || bandid == null)
            {
                return NotFound();
            }

            var participation = await _context.Participations
                .Include(p => p.Band)
                .Include(p => p.Event)
                .SingleOrDefaultAsync(m => m.EventID == eventid && m.BandID == bandid);
            if (participation == null)
            {
                return NotFound();
            }

            return View(participation);
        }

        /// <summary>
        /// Deletes the participation with concatenated key and redirect to Index
        /// </summary>
        /// <param name="eventid">The EventID of the participation to delete</param>
        /// <param name="bandid">The BandID of the participation to delete</param>
        /// <returns>Redirect to Index if succeed or else display the view with validations errors</returns>
        // POST: Participations/Delete/6/9
        [HttpPost]
        [Route("/Participations/Delete/{eventid}/{bandid}")]
        public async Task<IActionResult> DeleteConfirmed(int eventid, int bandid)
        {
            if (_context.Bands == null || _context.Events == null || _context.Participations == null)
                return View();

            var participation = await _context.Participations.SingleOrDefaultAsync(m => m.EventID == eventid && m.BandID == bandid);
            _context.Participations.Remove(participation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Check if a participation exists with the given concatenated key
        /// </summary>
        /// <param name="eventID">The EventID of the participation to check </param>
        /// <param name="bandID">The BandID of the participation to check </param>
        /// <returns>true if the participation exists or else false</returns>
        private bool ParticipationExists(int eventID, int bandID)
        {
            return _context.Participations.Any(e => e.EventID == eventID && e.BandID == bandID);
        }
    }
}
