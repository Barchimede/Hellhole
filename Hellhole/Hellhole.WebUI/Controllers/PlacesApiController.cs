﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;

namespace Hellhole.WebUI.Controllers
{
    [Produces("application/json")]
    [Route("api/Places")]
    public class PlacesApiController : Controller
    {
        private readonly HellholeDbContext _context;

        public PlacesApiController(HellholeDbContext context)
        {
            _context = context;
        }

        // GET: api/Places

        /// <summary>
        /// Get all the places from the DB
        /// </summary>
        /// <returns>All the places in the DB</returns>
        [HttpGet]
        public IEnumerable<Place> GetPlaces()
        {
            if (_context.Places == null)
                return null;

            return _context.Places;
        }

        // GET: api/Places/5

        /// <summary>
        /// Get the place with the given id
        /// </summary>
        /// <param name="id">The placeID of the Place required</param>
        /// <returns>The place found or else a bad request</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlace([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_context.Places == null)
                return null;

            var place = await _context.Places.SingleOrDefaultAsync(m => m.PlaceID == id);

            if (place == null)
            {
                return NotFound();
            }

            return Ok(place);
        }
    }
}