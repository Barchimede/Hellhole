﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Hellhole.WebUI.Data;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using System.Net.Http;
using Hellhole.WebUI.Models;
using Newtonsoft.Json;

namespace Hellhole.WebUI.Controllers
{
    [Authorize]
    public class PlacesController : Controller
    {
        private readonly HellholeDbContext _context;

        public PlacesController(HellholeDbContext context)
        {
            _context = context;
        }

        // GET: Places

        /// <summary>
        /// Get the view to see all the places of the application
        /// </summary>
        /// <returns>the view required</returns>
        public async Task<IActionResult> Index()
        {
            if (_context.Places == null)
                return View();

            return View(await _context.Places.ToListAsync());
        }

        // GET: Places/Create

        /// <summary>
        /// Get the view to create a place
        /// </summary>
        /// <returns>The view required</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Places/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        /// <summary>
        /// Creates a new place
        /// </summary>
        /// <param name="placeParam">The place that will be created</param>
        /// <returns>Redirect to Index if succeeded or else send the error messages</returns>
        [HttpPost]
        public async Task<IActionResult> Create([Bind("PlaceID,PostalCode,City,Street,Number,Commune,Mailbox")] Place placeParam = null)
        {
            if (placeParam != null && ModelState.IsValid)
            {

                if(! await CheckExistsAsync(placeParam))
                {
                    ModelState.AddModelError(string.Empty, "The place doesn't exists.");
                    return View(placeParam);
                }

                _context.Add(placeParam);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(placeParam);
        }

        // GET: Places/Details/5

        /// <summary>
        /// Get the details of a place 
        /// </summary>
        /// <param name="id">The id of the place to find</param>
        /// <returns>The view of the place if found or else the NotFound page</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (_context.Places == null)
                return View();

            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places
                .SingleOrDefaultAsync(m => m.PlaceID == id);
            if (place == null)
            {
                return NotFound();
            }

            return View(place);
        }

        // GET: Places/Edit/5

        /// <summary>
        /// Get the page to edit a place
        /// </summary>
        /// <param name="id">The PlaceID of the place to edit</param>
        /// <returns>NotFound page if not found or else the view required</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var place = await _context.Places.SingleOrDefaultAsync(m => m.PlaceID == id);
            if (place == null)
            {
                return NotFound();
            }
            return View(place);
        }

        // POST: Places/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        /// <summary>
        /// Edits the place
        /// </summary>
        /// <param name="id">the id of the place to edit</param>
        /// <param name="place">The new values of the place to edit</param>
        /// <returns>Redirect to Index if succeeded, or else send the error messages</returns>
        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("PlaceID,PostalCode,City,Street,Number,Commune,Mailbox")] Place place)
        {
            if (id != place.PlaceID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                if (!await CheckExistsAsync(place))
                {
                    ModelState.AddModelError(string.Empty, "The place doesn't exists.");
                    return View(place);
                }

                try
                {
                    _context.Update(place);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlaceExists(place.PlaceID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(place);
        }

        /// <summary>
        /// Checks whether the place really exists thanks to Google maps
        /// </summary>
        /// <param name="place">the place to check</param>
        /// <returns>true if the place exists or else false</returns>
        private async Task<bool> CheckExistsAsync(Place place)
        {
            string adr = place.PostalCode + "," + place.Street + "," + place.Number + "," + place.Commune + "," + place.City,
                cle = "AIzaSyDrkgUtF_6SwhhQnv5T6ryq2eRdSDgyGe0";

            StringBuilder builder = new StringBuilder(adr);
            builder.Replace(' ', '+');
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("https://maps.googleapis.com");
                    var response = await client.GetAsync($"/maps/api/geocode/json?address={builder.ToString()}&key={cle}");
                    response.EnsureSuccessStatusCode();
                    var stringResult = await response.Content.ReadAsStringAsync();
                    GeocoMapsResponse rawWeather = JsonConvert.DeserializeObject<GeocoMapsResponse>(stringResult);
                    return rawWeather.Status.Equals(GeocoMapsResponse.OK_CODE);
                }
                catch (HttpRequestException httpRequestException)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Checks whether the place exists in the database
        /// </summary>
        /// <param name="id">The placeID to check</param>
        /// <returns></returns>
        private bool PlaceExists(int id)
        {
            return _context.Places.Any(e => e.PlaceID == id);
        }
    }
}
