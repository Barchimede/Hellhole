﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.WebUI.Data
{
    public partial class Band
    {
        public Band()
        {
            Participations = new HashSet<Participation>();
        }
        [Required]
        public int BandID { get; set; }

        [Required(ErrorMessage = "Free Lacrim")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters")]
        public string Name { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Style cannot be longer than 20 characters. Even if your satanic music has kilometric names, I don't give a fuck, try to like pop music instead, you little piece of shit")]
        public string Style { get; set; }

        [Required]
        [StringLength(50,ErrorMessage = "Origin cannot be longer than 50 characters.")]
        public string Origin { get; set; }

        [Display(Name = "Spotify ID")]
        public string LinkSpotify { get; set; }

        [Display(Name = "Youtube video ID")]
        public string LinkYoutube { get; set; }

        [Display(Name = "Bandcamp link")]
        public string LinkBandcamp { get; set; }

        [Display(Name="SoundCloud ID")]
        public string LinkSoundCloud { get; set; }

        [Required]
        [Display(Name = "Band image")]
        public string LinkImage { get; set; }

        [Required]
        [Display(Name="Facebook page url")]
        public string LinkFacebook { get; set; }

        [Required]
        public string Description { get; set; }

        public ICollection<Participation> Participations { get; set; }
    }
}
