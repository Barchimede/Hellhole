﻿using Hellhole.WebUI.Models.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.WebUI.Data
{
    public partial class Event
    {
        public Event()
        {
            Participations = new HashSet<Participation>();
        }
        public const string Party = "Party";
        public const string Concert = "Concert";

        public int EventID { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DoorsLessThanCurfew(ErrorMessage = "Doors has to be before curfew")]
        public DateTime Doors { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Curfew { get; set; }

        [Required]
        [StringLength(50,ErrorMessage = "Title cannot be longer than 50 characters.")]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Range(0, int.MaxValue, ErrorMessage = "Price cannot be negative.")]
        public decimal Price { get; set; }

        [Required]
        [RegularExpression(@"^(Party)|(Concert)$",ErrorMessage = "Type is either \"Party\" or \"Concert\"")]
        [StringLength(7)]
        public string Type { get; set; }

        [Required]
        [StringLength(20,ErrorMessage = "Style cannot be longer than 20 characters. Even if your satanic music has kilometric names, I don't give a fuck, try to like pop music instead, you little piece of shit")]
        public string Style { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Event image")]
        public string LinkImage { get; set; }

        [Required]
        public bool Canceled { get; set; }

        [Required]
        public int PlaceID { get; set; }

        public Place Place { get; set; }
        public ICollection<Participation> Participations { get; set; }
    }
}
