﻿using Hellhole.WebUI.Models.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.WebUI.Data
{
    public partial class Participation
    {
        [Display(Name = "Event")]
        [Required]
        public int EventID { get; set; }

        [Display(Name = "Band")]
        [Required]
        public int BandID { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [StartingLessThanEnding]
        public DateTime Starting { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Ending { get; set; }

        [Required]
        public bool Canceled { get; set; }

        public Band Band { get; set; }
        public Event Event { get; set; }
    }
}
