﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hellhole.WebUI.Data
{
    public partial class Place
    {
        public Place()
        {

            Events = new HashSet<Event>();
        }

        public int PlaceID { get; set; }

        [Required]
        [Display(Name = "Postal code")]
        [StringLength(20, ErrorMessage = "Postal code cannot be longer than 20 characters")]
        public string PostalCode { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "City cannot be longer than 20 characters")]
        public string City { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Street cannot be longer than 50 characters")]
        public string Street { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Number cannot be longer than 20 characters")]
        public string Number { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Commune cannot be longer than 50 characters")]
        public string Commune { get; set; }

        [StringLength(20, ErrorMessage = "Mailbox cannot be longer than 20 characters")]
        public string Mailbox { get; set; }

        public ICollection<Event> Events { get; set; }

        public override string ToString()
        {
            return $"{Number}, {Street} {City} - {PostalCode} {Commune} (Boite : {Mailbox})";
        }
    }
}
