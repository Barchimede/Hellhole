﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Hellhole.WebUI.Models;

namespace Hellhole.WebUI.Data
{
    public class HellholeDbContext : DbContext
    {
        public virtual DbSet<Band> Bands { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Participation> Participations { get; set; }
        public virtual DbSet<Place> Places { get; set; }

        public HellholeDbContext(DbContextOptions options) : base(options)
        {
        }

        public HellholeDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<HellholeDbContext>();
            optionsBuilder.UseInMemoryDatabase("hellhole.db");
            new HellholeDbContext(optionsBuilder.Options);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Participation>(options =>
            {
                options.HasKey(o => new { o.EventID, o.BandID });
                options.Property(b => b.Canceled).HasDefaultValue(false);
            });
            modelBuilder.Entity<Event>(options =>
            {
                options.Property(e => e.Canceled).HasDefaultValue(false);
            });
        }
    }
}
