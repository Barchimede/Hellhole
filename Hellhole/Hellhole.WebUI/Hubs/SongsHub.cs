﻿using Hellhole.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Hubs
{
    public class SongsHub : Hub
    {
        
        private readonly SongsStore _store;
        public SongsHub(SongsStore store)
        {
            _store = store;
        }

        /// <summary>
        /// Adds a new song to the store
        /// </summary>
        /// <param name="song">The song to add</param>
        /// <returns></returns>
        public Task Send(SongItem song)
        {
            _store.Send(song, Context.ConnectionId);
            return Clients.All.InvokeAsync("Send", _store.Songs);
        }

        /// <summary>
        /// Likes a song in the store
        /// </summary>
        /// <param name="song">The song to like</param>
        /// <returns></returns>
        public Task Like(SongItem song)
        {
            _store.Like(song, Context.ConnectionId);
            return Clients.All.InvokeAsync("Like", _store.Songs);
        }

        /// <summary>
        /// Unlikes a song in the store
        /// </summary>
        /// <param name="song">The song to unlike</param>
        /// <returns></returns>
        public Task Unlike(SongItem song)
        {
            _store.Unlike(song, Context.ConnectionId);
            return Clients.All.InvokeAsync("Unlike", _store.Songs);
        }

        /// <summary>
        /// Removes a song from the store
        /// </summary>
        /// <param name="song">The SongItem to remove </param>
        /// <returns></returns>
        [Authorize]
        public Task Remove(SongItem song)
        {
            _store.Remove(song, Context.ConnectionId);
            return Clients.All.InvokeAsync("Remove", _store.Songs);
        }

        /// <summary>
        /// Websocket method that tells the server a user has joined
        /// </summary>
        /// <returns></returns>
        public Task Join()
        {
            return Clients.Client(Context.ConnectionId).InvokeAsync("Join", _store.Songs);
        }

        /// <summary>
        /// Resets the store. Admin method.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public Task Reinit()
        {
            _store.Reinit();
            return Clients.All.InvokeAsync("Reinit");
        }

        public override Task OnConnectedAsync() 
        {
            _store.Join(Context.ConnectionId);
            Join();
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _store.Disconnect(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
    }
}
