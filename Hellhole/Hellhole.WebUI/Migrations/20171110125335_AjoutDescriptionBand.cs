﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Hellhole.WebUI.Migrations
{
    public partial class AjoutDescriptionBand : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Places_PlaceID",
                table: "Events");

            migrationBuilder.AlterColumn<int>(
                name: "PlaceID",
                table: "Events",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Bands",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Places_PlaceID",
                table: "Events",
                column: "PlaceID",
                principalTable: "Places",
                principalColumn: "PlaceID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Places_PlaceID",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Bands");

            migrationBuilder.AlterColumn<int>(
                name: "PlaceID",
                table: "Events",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Places_PlaceID",
                table: "Events",
                column: "PlaceID",
                principalTable: "Places",
                principalColumn: "PlaceID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
