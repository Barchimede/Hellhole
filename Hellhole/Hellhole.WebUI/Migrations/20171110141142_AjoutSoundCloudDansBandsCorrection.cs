﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Hellhole.WebUI.Migrations
{
    public partial class AjoutSoundCloudDansBandsCorrection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkSoundClound",
                table: "Bands");

            migrationBuilder.AddColumn<string>(
                name: "LinkSoundCloud",
                table: "Bands",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkSoundCloud",
                table: "Bands");

            migrationBuilder.AddColumn<string>(
                name: "LinkSoundClound",
                table: "Bands",
                nullable: true);
        }
    }
}
