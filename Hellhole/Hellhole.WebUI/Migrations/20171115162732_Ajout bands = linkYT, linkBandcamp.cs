﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Hellhole.WebUI.Migrations
{
    public partial class AjoutbandslinkYTlinkBandcamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LinkBandcamp",
                table: "Bands",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LinkYoutube",
                table: "Bands",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LinkBandcamp",
                table: "Bands");

            migrationBuilder.DropColumn(
                name: "LinkYoutube",
                table: "Bands");
        }
    }
}
