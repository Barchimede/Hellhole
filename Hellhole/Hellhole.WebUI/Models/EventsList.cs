﻿using Hellhole.WebUI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models
{
    public class EventsList
    {
        public IEnumerable<Event> All { get; set; }

        public IEnumerable<Event> Concerts { get; set; }

        public IEnumerable<Event> Parties { get; set; }
    }
}
