﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models
{
    public class GeocoMapsResponse
    {
        public const string OK_CODE = "OK";

        public IEnumerable<Object> Results { get; set; }
        public string Status { get; set; }
    }
}
