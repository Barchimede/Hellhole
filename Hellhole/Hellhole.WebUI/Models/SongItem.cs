﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models
{
    public class SongItem
    {
        public string Artiste { get; set; }
        public string Titre { get; set; }
        public int Like { get; set; }
        public string Creator { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as SongItem;
            return item != null &&
                   Artiste == item.Artiste &&
                   Titre == item.Titre;
        }

        public override int GetHashCode()
        {
            var hashCode = -999992164;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Artiste);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Titre);
            return hashCode;
        }
    }
}
