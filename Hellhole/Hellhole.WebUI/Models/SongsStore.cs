﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models
{
    public class SongsStore
    {
        private const int maxSongs = 2;
        private HashSet<SongItem> _songs = new HashSet<SongItem>();
        private IDictionary<string, SongItem> _likers = new Dictionary<string, SongItem>();
        private IDictionary<string, int> _songsPerUser = new Dictionary<string, int>();
    
        public HashSet<SongItem> Songs { get { return _songs; } }

        public void Send(SongItem song, string connectionId)
        {
            bool allowed = !_songs.Contains(song) && _songsPerUser.ContainsKey(connectionId) && _songsPerUser[connectionId] < maxSongs;
            if (!allowed) return;
            song.Creator = connectionId;
            song.Like = 0;
            _songsPerUser[connectionId]++;
            _songs.Add(song);
        }

        public void Like(SongItem song, string connectionId)
        {
            if (!_songs.TryGetValue(song, out SongItem liked)) return;
            liked.Like++;
            if(_likers.TryGetValue(connectionId, out SongItem abandoned))
            {
                abandoned.Like--;
                _likers[connectionId] = liked;
            }
            else
            {
                _likers.Add(connectionId, liked);
            }
        }

        public void Unlike(SongItem song, string connectionId)
        {
            if (!_songs.TryGetValue(song, out SongItem unliked) || 
                ! _likers.TryGetValue(connectionId,out SongItem likedBefore) || 
                ! likedBefore.Equals(unliked))
                return;

            unliked.Like--;
            _likers.Remove(connectionId);
        }

        public void Remove(SongItem song, string connectionId)
        {
            if (!_songs.TryGetValue(song, out SongItem removed)) return;
            if(_songsPerUser.ContainsKey(removed.Creator)) _songsPerUser[removed.Creator]--;
            _songs.Remove(removed);
            IList<string> toRemove = new List<string>();

            //Find the likers to remove
            foreach (string liker in _likers.Where(pair => pair.Value.Equals(removed)).Select(pair => pair.Key)){
                toRemove.Add(liker);
            }

            //Remove the likers
            foreach(string liker in toRemove)
            {
                _likers.Remove(liker);
            }

        }

        public void Join(string connectionId)
        {
            _songsPerUser.Add(connectionId, 0);
        }
        
        public void Reinit()
        {
            _songs.Clear();
            _likers.Clear();
            IList<string> keys = new List<string>(_songsPerUser.Keys);
            foreach (string key in keys) _songsPerUser[key] = 0;
        }

        public void Disconnect(string connectionId)
        {
            if (!_songsPerUser.ContainsKey(connectionId)) return;
            _songsPerUser.Remove(connectionId);
        }
    }
}
