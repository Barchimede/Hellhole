﻿using Hellhole.WebUI.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models.Validation
{
    public class DoorsLessThanCurfewAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Event ev = (Event)validationContext.ObjectInstance;
            if (ev.Doors.CompareTo(ev.Curfew) < 0) return ValidationResult.Success;
            else return new ValidationResult(ErrorMessage);
        }
    }
}
