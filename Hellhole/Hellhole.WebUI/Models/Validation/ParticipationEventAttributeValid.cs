﻿using Hellhole.WebUI.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models.Validation
{
    public class ParticipationEventAttributeValid : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Participation participation = (Participation) validationContext.ObjectInstance;
            Event ev = participation.Event;
            if (ev == null) return ValidationResult.Success;
            if (ev.Canceled == true)
                return new ValidationResult("You cannot create a participation to a canceled event.");
            if (ev.Type.Equals("Party"))
                return new ValidationResult("You cannot create a participation to a party.");
            if (participation.Starting.CompareTo(ev.Doors) < 0 || participation.Starting.CompareTo(ev.Curfew) > 0)
                return new ValidationResult("You cannot create a participation that starts before/after its event.");
            if(participation.Ending.CompareTo(ev.Curfew) > 0 || participation.Ending.CompareTo(ev.Doors) < 0)
                return new ValidationResult("You cannot create a participation that ends before/after its event's doors/curfew.");
            if (participation.Canceled)
                return new ValidationResult("You can't create a canceled participation.");
            else return ValidationResult.Success;
        }
    }
}
