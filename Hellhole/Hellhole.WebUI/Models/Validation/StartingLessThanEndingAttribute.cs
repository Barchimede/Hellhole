﻿using Hellhole.WebUI.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Hellhole.WebUI.Models.Validation
{
    public class StartingLessThanEndingAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Participation pa = (Participation)validationContext.ObjectInstance;
            if (pa.Starting.CompareTo(pa.Ending) < 0) return ValidationResult.Success;
            else return new ValidationResult(ErrorMessage);
        }
    }
}
