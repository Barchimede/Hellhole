﻿CREATE TABLE "Participations"
(
	"Event" INT NOT NULL,
	"Band" INT NOT NULL,
	"Starting" DATETIME NOT NULL,
	"Ending" DATETIME NOT NULL,
	"Canceled" BIT NOT NULL, 
	CONSTRAINT [FK_Participations_Bands] FOREIGN KEY ("Band") REFERENCES "Bands"("BandID"), 
    CONSTRAINT [FK_Participations_Events] FOREIGN KEY ("Event") REFERENCES "Events"("EventID"),
    CONSTRAINT [PK_Participations] PRIMARY KEY ("Band","Event")
    
)
