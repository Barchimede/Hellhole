﻿INSERT INTO dbo.Places(PostalCode,City,Commune,Mailbox,Number,Street)
VALUES 
	('1160','Brussels','Auderghem',NULL,'23a','Avenue Benjamin Jansen'),
	('1150','Brussels','Watermael Boitsfort',NULL,'69','Rue des trois moineaux'),
	('1050','Brussels','Etterbeek',NULL,'42','Place Flagey');

INSERT INTO dbo.Bands (Name,Origin,Style,LinkSpotify,LinkFacebook,LinkSoundCloud,LinkImage, Description)
VALUES
	('Les Sombres','BE','Metal',NULL,'https://www.facebook.com/HellholeProject/',NULL,'\Content\Bands\band1test.jpg',' Ithilien is a Belgian metal band defining itself as FolkCore to underline the combination of traditional Belgian (folk) music, played with Flemish bagpipes, a hurdy gurdy, a violin and a bouzouki ; and a modern metal genre (death metal, metalcore). They are ready to introduce to you their brand new album ''Shaping the Soul'' !'),
	('The darks','EN','Metal',NULL,'https://www.facebook.com/HellholeProject/',NULL,'\Content\Bands\band2test.jpg','With a mix of epic pagan metal and typical catchy folk metal, Vanaheim''s energetic but vivacious sound makes you want to move, whether it''s headbang, mosh or straight out partying !'),
	('De donkers','NE','Metal',NULL,'https://www.facebook.com/HellholeProject/',NULL,'\Content\Bands\band3test.jpg','A long time ago, many warriors lived on our lands. Some ruled the oceans, others colonized the lands. If one day these men were to meet, it would be the most epic battle of all time, worthy of the History books ... These are the tales of Trikhorn - Folk Pirate Metal, speaking of unlikely meetings between vikings and pirates coming together to conquer the world !');

INSERT INTO dbo.Events (Curfew,Doors,Description,Canceled,LinkImage,Price,Style,Title,Type,PlaceID)
VALUES
	('20171023 20:00:00.00','20171023 12:00:00.00','Here is the Hellhole Project''s second installment of this serie of concerts in collaboration with the Jeugdhuis De Schakel. This time, four bands, from punk rock to hardcore, and still the same bargained price of only 2 € !',0,'\Content\Events\event1test.jpg',02.00,'Metal','Smash it and quit it #4','Concert',8 ),
	('20171023 20:00:00.00','20171023 12:00:00.00','Get ready for an amazing autumn party when the Circle 3 will open, thanks to the combination of Ithilien, Vanaheim, Trikhorn - Folk Pirate Metal, and Coliseum. Our second folk metal show will make you bang your head and singalong till late in the night !',1,'\Content\Events\event2test.jpg',05.00,'Metal','Nut and go #2','Party',9 );


INSERT INTO dbo.Participations (BandID,EventID,Starting,Ending,Canceled)
VALUES
	(10,1006,'20171023 12:00:00.00','20171023 15:00:00.00',0),
	(11,1006,'20171023 15:00:00.00','20171023 20:00:00.00',1);