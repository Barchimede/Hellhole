﻿

var connection = new signalR.HubConnection('/Songs'), songLiked;

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

var songTable = new Vue({
    el: '#song-table',
    data: {
        songs: [],
        liked: null
    },
    methods: {
        like: function (song) {
            connection.invoke('Like', song);
            songTable.liked = getSongId(song);
        },
        unlike: function (song) {
            connection.invoke('Unlike', song);
            songTable.liked = null;
        },
        remove: function (song) {
            connection.invoke('Remove', song);
            if (songTable.liked && getSongId(song) === songTable.liked) songTable.liked = null;
        }
    }
});
var songForm = new Vue({
    el: '#song-form',
    data: {
        song: {
            artiste: '',
            titre: '',
            like:0
        },
        proposed: []
    },
    methods: {
        send: function () {
            console.log(songForm.song);
            if (songForm.proposed.length >= 2) {
                toastr["warning"]("You cannot send more than 2 songs", "Warning !");
                return;
            }
            if (songTable.songs.filter(son => getSongId(son) === getSongId(songForm.song)).length != 0) {
                toastr["warning"]("You cannot send a song that already exists.", "Warning !");
                return;
            }
            songForm.proposed.push(getSongId(songForm.song));
            connection.invoke('Send', songForm.song);
        },
        reset: function () {
            connection.invoke('Reinit');
        }
    }
});

function getSongId(song) {
    return song.artiste + '_' + song.titre;
}
function checkRemove(songs) {
    songForm.proposed = songs.filter(son => songForm.proposed.indexOf(getSongId(son)) != -1).map(son => getSongId(son));
    if (songs.filter(song => getSongId(song) === songTable.liked).length === 0) {
        songTable.liked = null;
    }
}
function displaySongs(songs) {
    songs.sort(function (songA, songB) {
        return songB.like - songA.like;
    });
    songTable.songs = songs;
}

connection.on('Join', function (data) {
    displaySongs(data);
});
connection.on('Remove', function (data) {
    checkRemove(data);
    displaySongs(data);
});
connection.on('Send', function (data) {
    displaySongs(data);
});

connection.on('Like', function (data) {
    displaySongs(data);
});

connection.on('Unlike', function (data) {
    displaySongs(data);
});

connection.on('Reinit', function () {
    displaySongs([]);
    songForm.proposed = [];
    songTable.liked = null;
});

connection.start();


